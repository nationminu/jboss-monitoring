# 1. JSTAT 명령어

> https://docs.oracle.com/javase/8/docs/technotes/guides/troubleshoot/tooldescr017.html#:~:text=The%20jstat%20utility%20uses%20the,heap%20sizing%20and%20garbage%20collection. <BR>
> S0 : Survivor 영역 0 의 사용율 (현재의 용량에 대한 퍼센티지) <BR>
> S1 : Survivor 영역 1 의 사용율 (현재의 용량에 대한 퍼센티지) <BR>
> E : Eden 영역의 사용율 (현재의 용량에 대한 퍼센티지) <BR>
> O : Old 영역의 사용율 (현재의 용량에 대한 퍼센티지) <BR>
> M(P) : Meta(Permanent) 영역의 사용율 (현재의 용량에 대한 퍼센티지) <BR>
> YGC : Young 세대의 GC 이벤트수 <BR>
> YGCT : Young 세대의 가베지 콜렉션 시간 <BR>
> FGC : 풀 GC 이벤트수 <BR>
> FGCT : 풀 가베지 콜렉션 시간 <BR>
> GCT : 가베지 콜렉션총시간 <BR>
> FGC 평균시간 = FGCT / FGC

```
jstat -gcutil <PID> 
  S0     S1     E      O      M     CCS    YGC     YGCT    FGC    FGCT     GCT
 53.12   0.00  38.90  32.93  92.34  84.07    354   33.291     0    0.000   33.291  
```

# 2. EAP6 성능 파라미터
## 1) JBoss CLI 접속(local)
```
./jboss-cli.sh --connect
```

## 2) Thread 사용율

> https://access.redhat.com/solutions/304133  <BR>
> active-count	실행 중인 스레드 수 <BR>
> max-threads 동시에 처리 가능한 thread 개수 <BR>
> largest-thread-count 현재까지 풀에 있었던 스레드의 최대 개수 

```
/subsystem=threads/unbounded-queue-thread-pool=ajp-thread:read-resource(include-runtime=true)
{
    "outcome" => "success",
    "result" => {
        "active-count" => 0,
        "completed-task-count" => 0L,
        "current-thread-count" => 0,
        "keepalive-time" => undefined,
        "largest-thread-count" => 0,
        "max-threads" => 512,
        "name" => "ajp-thread",
        "queue-size" => 0,
        "rejected-count" => 0,
        "task-count" => 0L,
        "thread-factory" => undefined
    }
}

```
## 3) DB Connection Pool 사용율

> https://access.redhat.com/solutions/268793 <BR>
> ActiveCount 현재 사용 중인 연결 개수 <BR>
> AvailableCount 사용 가능한 연결 개수 

```
/subsystem=datasources/data-source=ExampleDS/statistics=pool:read-resource(include-runtime=true)
{
    "outcome" => "success",
    "result" => {
        "ActiveCount" => "0",
        "AvailableCount" => "0",
        "AverageBlockingTime" => "0",
        "AverageCreationTime" => "0",
        "CreatedCount" => "0",
        "DestroyedCount" => "0",
        "InUseCount" => "0",
        "MaxCreationTime" => "0",
        "MaxUsedCount" => "0",
        "MaxWaitCount" => "0",
        "MaxWaitTime" => "0",
        "TimedOut" => "0",
        "TotalBlockingTime" => "0",
        "TotalCreationTime" => "0",
        "statistics-enabled" => false
    }
}
```

# 3. EAP7 성능 파라미터
## 1) JBoss CLI 접속(local)
```
./jboss-cli.sh --connect
```

## 2) Thread 사용율
> https://access.redhat.com/solutions/2433971 <BR>
> busy-task-thread-count	실행 중인 스레드 수 <BR>
> task-max-threads 동시에 처리 가능한 thread 개수 (기본값 cpu * 16)
```
/] /subsystem=io/worker=default:read-resource(include-runtime=true)
{
    "outcome" => "success",
    "result" => {
        "busy-task-thread-count" => 0,
        "core-pool-size" => 2,
        "io-thread-count" => 8,
        "io-threads" => "8",
        "max-pool-size" => 64,
        "queue-size" => 0,
        "shutdown-requested" => false,
        "stack-size" => "0",
        "task-core-threads" => "2",
        "task-keepalive" => "60000",
        "task-max-threads" => "64",
        "outbound-bind-address" => undefined,
        ...
    }
}
```

## 3) DB Connection Pool 사용율

> https://access.redhat.com/solutions/268793 <BR>
> ActiveCount 현재 사용 중인 연결 개수 <BR>
> AvailableCount 사용 가능한 연결 개수

```
/] /subsystem=datasources/data-source=egov/statistics=pool:read-resource(include-runtime=true)
{
    "outcome" => "success",
    "result" => {
        "ActiveCount" => 0,
        "AvailableCount" => 0,
        "AverageBlockingTime" => 0L,
        "AverageCreationTime" => 0L,
        "AverageGetTime" => 0L,
        "AveragePoolTime" => 0L,
        "AverageUsageTime" => 0L,
        "BlockingFailureCount" => 0,
        "CreatedCount" => 0,
        "DestroyedCount" => 0,
        "IdleCount" => 0,
        "InUseCount" => 0,
        "MaxCreationTime" => 0L,
        "MaxGetTime" => 0L,
        "MaxPoolTime" => 0L,
        "MaxUsageTime" => 0L,
        "MaxUsedCount" => 0,
        "MaxWaitCount" => 0,
        "MaxWaitTime" => 0L,
        "TimedOut" => 0,
        "TotalBlockingTime" => 0L,
        "TotalCreationTime" => 0L,
        "TotalGetTime" => 0L,
        "TotalPoolTime" => 0L,
        "TotalUsageTime" => 0L,
        "WaitCount" => 0,
        "XACommitAverageTime" => 0L,
        "XACommitCount" => 0L,
        "XACommitMaxTime" => 0L,
        "XACommitTotalTime" => 0L,
        "XAEndAverageTime" => 0L,
        "XAEndCount" => 0L,
        "XAEndMaxTime" => 0L,
        "XAEndTotalTime" => 0L,
        "XAForgetAverageTime" => 0L,
        "XAForgetCount" => 0L,
        "XAForgetMaxTime" => 0L,
        "XAForgetTotalTime" => 0L,
        "XAPrepareAverageTime" => 0L,
        "XAPrepareCount" => 0L,
        "XAPrepareMaxTime" => 0L,
        "XAPrepareTotalTime" => 0L,
        "XARecoverAverageTime" => 0L,
        "XARecoverCount" => 0L,
        "XARecoverMaxTime" => 0L,
        "XARecoverTotalTime" => 0L,
        "XARollbackAverageTime" => 0L,
        "XARollbackCount" => 0L,
        "XARollbackMaxTime" => 0L,
        "XARollbackTotalTime" => 0L,
        "XAStartAverageTime" => 0L,
        "XAStartCount" => 0L,
        "XAStartMaxTime" => 0L,
        "XAStartTotalTime" => 0L,
        "statistics-enabled" => false
    }
}
```
